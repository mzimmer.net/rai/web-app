module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	plugins: [
		'@typescript-eslint',
	],
	settings: {
		react: {
			version: "detect"
		}
	},
	extends: [
		'eslint:recommended',
		'plugin:react/recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
	],
	rules: {
		"react/prop-types": "off",
		"no-mixed-spaces-and-tabs": "off",
		"sort-imports": ["error", {
			"ignoreCase": true
		}]
	}
}