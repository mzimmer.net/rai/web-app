import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {field, object, Parser, recursive, runParserEx, thing} from "ununknown";
import {payerActiveStateUnparser, payerIdUnparser, payerNameUnparser} from "../unparsers/payersUnparser";
import headersWithLocationParser from "../parsers/headersWithLocationParser";
import {Map} from "immutable";
import {PayerActiveState} from "../domain/types/PayerActiveState";
import {PayerId} from "../domain/types/PayerId";
import {PayerName} from "../domain/types/PayerName";
import payersParser from "../parsers/payersParser";

export type PayerWithoutId = { name: PayerName; activeState: PayerActiveState };
export type Payers = Map<PayerId, PayerWithoutId>
export type Payer = { id: PayerId } & PayerWithoutId;

interface PayersResponse {
	data: {
		payers: Payers;
	};
}

const payersResponseParser: Parser<PayersResponse, unknown> = recursive(() =>
	object.of({
		data: field.required(
			"data",
			object.of({
				payers: field.required("payers", payersParser)
			})
		)
	})
)

type ApiType = {
	enterReceipt: {
		payer: (query: string) => Promise<Payers>;
	};
	payers: {
		index: () => Promise<Payers>;
		create: (payerName: PayerName) => Promise<Payer>;
		updateName: (payerId: PayerId, payerName: PayerName) => Promise<void>;
		updateActiveState: (payerId: PayerId, payerActiveState: PayerActiveState) => Promise<void>;
	};
};


const idFromHeaders =
	(headers: unknown): string => {
		const headersWithLocation = runParserEx(headersWithLocationParser, headers);
		const pathElements = headersWithLocation.location.pathname.split("/");
		return runParserEx(thing.is.string, pathElements.pop());
	}

const request = async (request: AxiosRequestConfig): Promise<AxiosResponse<unknown>> => {
	return axios(request)
		.catch(error => {
			if (error.response) {
				// The request was made and the server responded with a status code
				// that falls out of the range of 2xx
				console.log(error.response.data);
				console.log(error.response.status);
				console.log(error.response.headers);
			} else if (error.request) {
				// The request was made but no response was received
				// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
				// http.ClientRequest in node.js
				console.log(error.request);
			} else {
				// Something happened in setting up the request that triggered an Error
				console.log('Error', error.message);
			}
			console.log(error.config);
			return Promise.reject(error)
		});
};

const build = (baseUri: string): ApiType => ({
	enterReceipt: {
		payer: async (query: string): Promise<Payers>
	},
	payers: {
		index: async (): Promise<Payers> => {
			const res = await request({
				baseURL: baseUri,
				url: "/payers"
			});
			const body = runParserEx(payersResponseParser, res.data)
			return body.data.payers;
		},
		create: async (payerName: PayerName): Promise<Payer> => {
			const res = await request({
				baseURL: baseUri,
				url: "/payers",
				method: "post",
				data: {
					data: {
						payer: {
							name: payerNameUnparser(payerName)
						}
					}
				}
			});
			const id = idFromHeaders(res.headers);
			return {
				id: id as PayerId,
				name: payerName,
				activeState: PayerActiveState.Active
			};
		},
		updateName: async (payerId: PayerId, payerName: PayerName): Promise<void> => {
			await request({
				baseURL: baseUri,
				url: `/payers/${payerIdUnparser(payerId)}/name`,
				method: "put",
				data: {
					data: {
						payer: {
							name: payerNameUnparser(payerName)
						}
					}
				}
			});
		},
		updateActiveState: async (payerId: PayerId, payerActiveState: PayerActiveState): Promise<void> => {
			await request({
				baseURL: baseUri,
				url: `/payers/${payerIdUnparser(payerId)}/activeState`,
				method: "put",
				data: {
					data: {
						payer: {
							activeState: payerActiveStateUnparser(payerActiveState)
						}
					}
				}
			});
		}
	}
});

const Api = {
	INSTANCE: build("http://localhost:8080")
}

export default Api