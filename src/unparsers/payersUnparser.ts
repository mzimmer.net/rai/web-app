import {PayerActiveState} from "../domain/types/PayerActiveState";
import {PayerId} from "../domain/types/PayerId";
import {PayerName} from "../domain/types/PayerName";
import {Payers} from "../api/Api";

export const payerIdUnparser = (payerId: PayerId): string => payerId;
export const payerNameUnparser = (payerName: PayerName): string => payerName;
export const payerActiveStateUnparser = (payerActiveState: PayerActiveState): string => payerActiveState === PayerActiveState.Active ? "active" : "inactive";

const payersUnparser = (payers: Payers): object =>
	payers.entrySeq()
		.reduce(
			(obj, [payerId, {activeState, name}]) =>
				({
					[payerIdUnparser(payerId)]: {
						name: payerNameUnparser(name),
						activeState: payerActiveStateUnparser(activeState)
					},
					...obj
				}),
			{}
		)

export default payersUnparser