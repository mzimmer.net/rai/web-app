import Api, {Payer} from "../api/Api";
import {Box, Button, Form, FormField} from "grommet";
import React, {FormEvent, FormEventHandler, useEffect, useState} from "react";
import {PayerName} from "../domain/types/PayerName";

interface Props {
	onAdd: (payer: Payer) => void;
}

enum Kind { Idle, Saving, Waiting}

const kindUnparser = (kind: Kind): string => {
	switch (kind) {
		case Kind.Idle:
			return "<idle>";
		case Kind.Saving:
			return "<saving>";
		case Kind.Waiting:
			return "<waiting>";
	}
}

interface IdleState {
	kind: Kind.Idle;
}

interface SavingState {
	kind: Kind.Saving;
	name: PayerName;
}

interface WaitingState {
	kind: Kind.Waiting;
}

type State
	= IdleState
	| SavingState
	| WaitingState;

const AddPayerForm: React.FC<Props> = ({onAdd}) => {
	const [state, setState] = useState<State>({kind: Kind.Idle});

	const createSuccessHandler =
		(payer: Payer): void => {
			onAdd(payer);
			if (state.kind === Kind.Waiting) {
				setState({
					kind: Kind.Idle
				});
			} else {
				console.error(`Tried to handle save success while in state ${kindUnparser(state.kind)}`);
			}
		}

	const createErrorHandler =
		(e: unknown): void => {
			console.error(e);
			if (state.kind === Kind.Waiting) {
				setState({
					kind: Kind.Idle
				});
			} else {
				console.error(`Tried to handle save error while in state ${kindUnparser(state.kind)}`);
			}
		}

	useEffect(() => {
		if (state.kind === Kind.Saving) {
			setState({
				kind: Kind.Waiting
			});
			Api.INSTANCE.payers.create(state.name)
				.then(createSuccessHandler, createErrorHandler);
		}
	})

	const formSubmitHandler: FormEventHandler =
		(e: FormEvent & { value: { name: PayerName } }): void => {
			if (state.kind === Kind.Idle) {
				setState({
					kind: Kind.Saving,
					name: e.value.name
				});
			} else {
				console.error(`Tried to save while in state ${kindUnparser(state.kind)}`);
			}
		};

	return <Form onSubmit={formSubmitHandler}>
		<FormField name="name"
				   label="Name for new payer"
				   required
				   disabled={state.kind != Kind.Idle}/>
		<Box direction="row"
			 justify="end">
			<Button type="submit"
					primary
					label="Add"
					disabled={state.kind != Kind.Idle}/>
		</Box>
	</Form>;
}

export default AddPayerForm;