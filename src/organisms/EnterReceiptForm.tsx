import {Box, Button, Form, FormField, RadioButtonGroup} from "grommet";
import FiniteSearchInput, {Displayable} from "../atoms/FiniteSearchInput";
import {Key, Keyable} from "../atoms/FiniteTagInput";
import {List, Map} from "immutable";
import React, {useState} from "react";
import StorePlusCategoriesInput, {WithCategories} from "../molecules/StorePlusCategoriesInput";
import MoneyInput from "../atoms/MoneyInput";

async function fakeCategoriesSearch(searchTerm: string): Promise<Map<Key, Displayable>> {
	console.log('fake category back end received search', searchTerm);
	const data: Array<[Key, Displayable]> = [
		["fake-id-household" as Key, {display: "Household"}],
		["fake-id-groceries" as Key, {display: "Groceries"}]
	];
	return Map(data);
}

async function fakeStorePlusCategoriesSearch(searchTerm: string): Promise<Map<Key, Displayable & WithCategories>> {
	console.log('fake store plus categories back end received search', searchTerm);
	const data: Array<[Key, Displayable & WithCategories]> = [
		["fake-id-shop-around-the-corner" as Key, {
			display: "Shop around the corner",
			categories: await fakeCategoriesSearch(searchTerm)
		}],
		["fake-id-pharmacy" as Key, {display: "Pharmacy", categories: Map()}]
	];
	return Map<Key, Displayable & WithCategories>(data);
}

async function fakePayerSearch(searchTerm: string): Promise<List<Keyable & Displayable>> {
	console.log('fake payer back end received search', searchTerm);
	return List([
		{key: "fake-id-alice" as Key, display: "Alice"},
		{key: "fake-id-bob" as Key, display: "Bob"}
	]);
}

const EnterReceiptForm: React.FC = () => {
	const [store, setStore] = useState<Keyable & Displayable | null>(null);
	const [categories, setCategories] = useState<Map<Key, Displayable>>(Map());
	const [payer, setPayer] = useState<Keyable & Displayable | null>(null);
	const [amount, setAmount] = useState<bigint>(BigInt(0));
	const [sharedPayment, setSharedPayment] = useState<"not" | "even">("not");

	return <Form onSubmit={(): void => {
		console.log('submit', JSON.stringify({
			store: store !== null ? store.key : null,
			categories: categories.keySeq().toArray(),
			payer: payer !== null ? payer.key : null,
			amount: amount.toString(),
			sharedPayment
		}))
	}}>
		<StorePlusCategoriesInput
			store={{
				autoFocus: true,
				value: store,
				onChange: setStore,
				search: fakeStorePlusCategoriesSearch
			}}
			categories={{
				value: categories,
				onAdd: (entry): void => setCategories(categories.set(entry.key, entry)),
				onAddMultiple: (entries): void => setCategories(categories.merge(entries)),
				onRemove: (entry): void => setCategories(categories.remove(entry.key)),
				search: fakeCategoriesSearch
			}}/>
		<Box direction="row-responsive"
			 justify="between">
			<FormField name="payer"
					   label="Payer">
				<FiniteSearchInput<Keyable> value={payer}
											onChange={setPayer}
											search={fakePayerSearch}/>
			</FormField>
			<MoneyInput name="amount"
						label="Amount"
						value={amount}
						onChange={setAmount}
						onKeyDown={(event): void => {
							if (event.shiftKey) {
								if (event.key === "N") {
									setSharedPayment("not");
									event.preventDefault();
								} else if (event.key === "E") {
									setSharedPayment("even");
									event.preventDefault();
								}
							}
						}}/>
		</Box>
		<Box margin={{top: 'large'}}>
			<RadioButtonGroup name="sharedPayment"
							  direction="row-responsive"
							  justify="evenly"
							  options={[
								  {label: "Not shared", value: "not"},
								  {label: "Evenly split", value: "even"}
							  ]}
							  value={sharedPayment}
							  onChange={(event): void => {
								  const value = (event.target.value === "even")
									  ? event.target.value
									  : "not";
								  setSharedPayment(value);
							  }}/>
		</Box>
		<Box direction="row"
			 justify="end"
			 margin={{top: 'large'}}>
			<Button type="submit"
					primary
					label="Submit"/>
		</Box>
	</Form>;
}

export default EnterReceiptForm