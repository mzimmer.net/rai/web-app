import Api, {PayerWithoutId} from "../api/Api";
import {useEffect, useState} from "react";
import {PayerActiveState} from "../domain/types/PayerActiveState";
import {payerActiveStateUnparser} from "../unparsers/payersUnparser";
import {PayerId} from "../domain/types/PayerId";
import {PayerName} from "../domain/types/PayerName";

export type InProgressName = PayerName & { readonly __tag: unique symbol };
type NewName = PayerName & { readonly __tag: unique symbol };
type OldName = PayerName & { readonly __tag: unique symbol };
export type NewActiveState = PayerActiveState & { readonly __tag: unique symbol };
type OldActiveState = PayerActiveState & { readonly __tag: unique symbol };

export enum Kind { Normal, Editing, SavingName, SavingActiveState }

const kindUnparser = (kind: Kind): string => {
	switch (kind) {
		case Kind.Normal:
			return "<normal>";
		case Kind.Editing:
			return "<editing>";
		case Kind.SavingName:
			return "<saving name>";
		case Kind.SavingActiveState:
			return "<saving active state>";
	}
}

type State
	= { kind: Kind.Normal; name: PayerName; activeState: PayerActiveState }
	| { kind: Kind.Editing; inProgressName: InProgressName; oldName: OldName; activeState: PayerActiveState }
	| { kind: Kind.SavingName; newName: NewName; oldName: OldName; activeState: PayerActiveState }
	| { kind: Kind.SavingActiveState; name: PayerName; newActiveState: NewActiveState; oldActiveState: OldActiveState };

interface UseManagePayerFormState {
	state: State;
	startEditing: () => void;
	updateName: (inProgressName: InProgressName) => void;
	cancelEditing: () => void;
	submitName: () => void;
	submitActiveState: (newActiveState: NewActiveState) => void;
}

const useManagePayerFormState = (id: PayerId, initialState: PayerWithoutId): UseManagePayerFormState => {
	const [state, setState] = useState<State>({kind: Kind.Normal, ...initialState});

	const startEditing =
		(): void => {
			if (state.kind === Kind.Normal) {
				setState({
					kind: Kind.Editing,
					inProgressName: state.name as InProgressName,
					oldName: state.name as OldName,
					activeState: state.activeState
				});
			} else {
				console.error(`Tried to go into edit mode while in state ${kindUnparser(state.kind)}`);
			}
		};

	const updateName =
		(inProgressName: InProgressName): void => {
			if (state.kind == Kind.Editing) {
				setState({
					kind: Kind.Editing,
					inProgressName: inProgressName,
					oldName: state.oldName,
					activeState: state.activeState
				});
			} else {
				console.error(`Tried to change name while in state ${kindUnparser(state.kind)}`);
			}
		};

	const cancelEditing =
		(): void => {
			if (state.kind === Kind.Editing) {
				setState({
					kind: Kind.Normal,
					name: state.oldName as PayerName,
					activeState: state.activeState
				});
			} else {
				console.error(`Tried to cancel edit mode while in state ${kindUnparser(state.kind)}`);
			}
		};

	const submitName =
		(): void => {
			if (state.kind === Kind.Editing) {
				if (state.inProgressName as PayerName !== state.oldName as PayerName) {
					setState({
						kind: Kind.SavingName,
						newName: state.inProgressName as NewName,
						oldName: state.oldName,
						activeState: state.activeState
					});
				} else {
					setState({
						kind: Kind.Normal,
						name: state.inProgressName as PayerName,
						activeState: state.activeState
					});
				}
			} else {
				console.error(`Tried to save name while in state ${kindUnparser(state.kind)}`);
			}
		};

	const useNewName =
		(): void => {
			if (state.kind === Kind.SavingName) {
				setState(
					{
						kind: Kind.Normal,
						name: state.newName as PayerName,
						activeState: state.activeState
					});
			} else {
				console.error(`Tried to handle saving name success while in state ${kindUnparser(state.kind)}`);
			}
		};

	const restoreOldName = (): void => {
		if (state.kind === Kind.SavingName) {
			setState(
				{
					kind: Kind.Normal,
					name: state.oldName as PayerName,
					activeState: state.activeState
				});
		} else {
			console.error(`Tried to handling saving name error while in state ${kindUnparser(state.kind)}`);
		}
	};

	const submitActiveState =
		(newActiveState: NewActiveState): void => {
			if (state.kind === Kind.Normal) {
				if (state.activeState !== (newActiveState as PayerActiveState)) {
					setState({
						kind: Kind.SavingActiveState,
						name: state.name,
						newActiveState,
						oldActiveState: state.activeState as OldActiveState
					})
				} else {
					console.error(`Active state is already ${payerActiveStateUnparser(state.activeState)}`);
				}
			} else {
				console.error(`Tried to toggle active state while in state ${kindUnparser(state.kind)}`);
			}
		};

	const useNewActiveState =
		(): void => {
			if (state.kind === Kind.SavingActiveState) {
				setState({
					kind: Kind.Normal,
					name: state.name,
					activeState: state.newActiveState as PayerActiveState
				});
			} else {
				console.error(`Tried to handling saving active state success while in state ${kindUnparser(state.kind)}`);
			}
		};

	const restoreOldActiveState =
		(): void => {
			if (state.kind === Kind.SavingActiveState) {
				setState({
					kind: Kind.Normal,
					name: state.name,
					activeState: state.oldActiveState as PayerActiveState
				});
			} else {
				console.error(`Tried to handling saving active state error while in state ${kindUnparser(state.kind)}`);
			}
		};

	//// Async handlers ////

	const saveActiveStateOnSuccessHandler = useNewActiveState;

	const saveActiveStateOnErrorHandler =
		(e: unknown): void => {
			console.error(e);
			restoreOldActiveState();
		};

	const saveNameOnSuccessHandler = useNewName;

	const saveNameOnErrorHandler =
		(e: unknown): void => {
			console.error(e);
			restoreOldName();
		};

	useEffect(() => {
		if (state.kind === Kind.SavingActiveState) {
			Api.INSTANCE.payers.updateActiveState(id, state.newActiveState as PayerActiveState)
				.then(saveActiveStateOnSuccessHandler, saveActiveStateOnErrorHandler);
		}
	})

	useEffect(() => {
		if (state.kind === Kind.SavingName) {
			Api.INSTANCE.payers.updateName(id, state.newName as PayerName)
				.then(saveNameOnSuccessHandler, saveNameOnErrorHandler);
		}
	});

	return {
		state,
		startEditing,
		updateName,
		cancelEditing,
		submitName,
		submitActiveState
	};
};

export default useManagePayerFormState;