import "../atoms/spin.css";
import {Box, Text} from "grommet";
import {Payer, Payers} from "../api/Api";
import React, {useState} from "react";
import AddPayerForm from "./AddPayerForm";
import ManagePayerForm from "./ManagePayerForm";

interface Props {
	preloadedPayers: Payers;
}

const ManagePayers: React.FC<Props> = ({preloadedPayers}) => {
	const [payers, setPayers] = useState<Payers>(preloadedPayers);

	const addHandler =
		(payer: Payer): void => {
			setPayers(payers.set(payer.id, payer));
		}

	return <>
		{
			payers.isEmpty() &&
			<Box align="center" margin={{top: "small"}}>
				<Text color="text-xweak">No payers registered</Text>
			</Box>
		}
		<>
			{payers.entrySeq().map(([payerId, payerWithoutId]) => (
				<ManagePayerForm key={payerId} id={payerId} preloaded={payerWithoutId}/>
			)).toArray()}
		</>
		<Box margin={{top: 'large'}}>
			<AddPayerForm onAdd={addHandler}/>
		</Box>
	</>;
}

export default ManagePayers