import "../atoms/spin.css";
import {Box, Button, CheckBox, Text, TextInput} from "grommet";
import {Checkmark, Clear, Edit, Refresh} from "grommet-icons";
import useManagePayerFormState, {InProgressName, Kind, NewActiveState} from "./ManagePayerFormState";
import {PayerActiveState} from "../domain/types/PayerActiveState";
import {PayerId} from "../domain/types/PayerId";
import {PayerWithoutId} from "../api/Api";
import React from "react";

interface Props {
	id: PayerId;
	preloaded: PayerWithoutId;
}

const ManagePayerForm: React.FC<Props> = ({id, preloaded}) => {

	const {
		state,
		startEditing,
		updateName,
		cancelEditing,
		submitName,
		submitActiveState
	} = useManagePayerFormState(id, preloaded);

	return <Box direction="row"
				justify="between"
				gap="small"
				pad={{horizontal: "medium", vertical: "small"}}>
		<Box direction="row">
			{state.kind === Kind.Editing || state.kind === Kind.SavingName ?
				<TextInput value={state.kind === Kind.Editing ? state.inProgressName : state.newName}
						   autoFocus={true}
						   disabled={state.kind === Kind.SavingName}
						   onChange={(e): void => {
							   const inProgressName = e.target.value as InProgressName;
							   updateName(inProgressName);
						   }}
						   onKeyDown={(e): void => {
							   if (e.key === "Escape") {
								   e.preventDefault();
								   cancelEditing();
							   } else if (e.key === "Enter") {
								   e.preventDefault();
								   submitName();
							   }
						   }}/> :
				<Text>{state.name}</Text>}
		</Box>
		<Box direction="row" gap="xsmall">
			{state.kind === Kind.Normal ?
				<Button plain={true}
						icon={<Edit/>}
						onClick={(e): void => {
							e.preventDefault();
							startEditing();
						}}/> :
				state.kind === Kind.Editing ?
					<>
						<Button plain={true}
								icon={<Checkmark/>}
								onClick={(e): void => {
									e.preventDefault();
									submitName();
								}}/>
						<Button plain={true}
								icon={<Clear/>}
								onClick={(e): void => {
									e.preventDefault();
									cancelEditing();
								}}/>
					</> :
					<Refresh color="status-disabled" style={{animation: "spin 1.2s linear infinite"}}/>}
			<CheckBox checked={(state.kind === Kind.SavingActiveState ? state.newActiveState : state.activeState) === PayerActiveState.Active}
					  toggle={true}
					  disabled={state.kind !== Kind.Normal}
					  onChange={(e): void => {
						  e.preventDefault();
						  const newActiveState = (e.target.checked ? PayerActiveState.Active : PayerActiveState.Inactive) as NewActiveState;
						  submitActiveState(newActiveState);
					  }}/>
		</Box>
	</Box>;
}

export default ManagePayerForm