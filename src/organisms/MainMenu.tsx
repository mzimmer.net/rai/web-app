import {Box, Button, Text} from "grommet";
import Link from "next/link";
import React from "react";
import {useRouter} from 'next/router';

const menuEntries: Array<{ path: string; name: string }> = [
	{path: "/", name: "Dashboard"},
	{path: "/enter-receipt", name: "Enter receipt"},
	{path: "/manage-household", name: "Manage household"}
];

const SelectableEntry: React.FC<{ path: string; name: string }> = ({path, name}) => (
	<Link href={path} key={path}>
		<Button hoverIndicator>
			<Box pad={{horizontal: "medium", vertical: "small"}}>
				<Text>{name}</Text>
			</Box>
		</Button>
	</Link>
)

const SelectedEntry: React.FC<{ name: string }> = ({name}) => (
	<Box pad={{horizontal: "medium", vertical: "small"}} background={{color: "active"}}>
		<Text>{name}</Text>
	</Box>
)

const MainMenu: React.FC = () => {
	const router = useRouter();

	return <Box tag={"nav"}>
		{menuEntries.map((menuEntry) =>
			router.pathname === menuEntry.path
				? <SelectedEntry key={menuEntry.path} {...menuEntry}/>
				: <SelectableEntry key={menuEntry.path} {...menuEntry}/>)}
	</Box>;
}

export default MainMenu