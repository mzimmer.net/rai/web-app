import {Heading} from "grommet";
import React from "react";

const Dashboard: React.FC = () => <Heading level="2">Dashboard</Heading>

export default Dashboard