import {compose, from, Parser, thing} from "ununknown";
import {either, Either, flatten, right} from "fp-ts/lib/Either";
import {Map} from "immutable";

const mapParser = <Key, Value, KeyError, ValueError, Input>(keyParser: Parser<Key, KeyError>, valueParser: Parser<Value, ValueError>): Parser<Map<Key, Value>, KeyError | ValueError | thing.is.TypeMismatchError, Input> =>
	compose(thing.is.object, from((o: object) =>
		Object.entries(o)
			.reduce(
				(acc: Either<KeyError | ValueError | thing.is.TypeMismatchError, Map<Key, Value>>, [key, value]: [string, unknown]) =>
					flatten(either.map(
						acc,
						(map: Map<Key, Value>) =>
							flatten(either.map(
								keyParser.runParser(key),
								parsedKey =>
									either.map<KeyError | ValueError, Value, Map<Key, Value>>(
										valueParser.runParser(value),
										parsedValue => map.set(parsedKey, parsedValue)
									)
							))
					)),
				right(Map<Key, Value>())
			)));

export default mapParser;