import {compose, field, from, object, Parser, recursive, thing} from "ununknown";
import {Payers, PayerWithoutId} from "../api/Api";
import mapParser from "./mapParser";
import {PayerActiveState} from "../domain/types/PayerActiveState";
import {PayerId} from "../domain/types/PayerId";
import {PayerName} from "../domain/types/PayerName";
import {right} from "fp-ts/lib/Either";

const payerIdParser: Parser<PayerId, thing.is.TypeMismatchError> = compose(thing.is.string, from((s: string) => right(s as PayerId)))

const payerNameParser: Parser<PayerName, thing.is.TypeMismatchError> = compose(thing.is.string, from((s: string) => right(s as PayerName)));

const payerActiveStateParser: Parser<PayerActiveState, thing.is.TypeMismatchError> = compose(thing.is.string, from((s: string) => right(s === "active" ? PayerActiveState.Active : PayerActiveState.Inactive)));

const payerWithoutIdParser: Parser<PayerWithoutId, unknown> = recursive(() =>
	object.of({
		name: field.required("name", payerNameParser),
		activeState: field.required("activeState", payerActiveStateParser),
	})
)

const payersParser: Parser<Payers, unknown> = mapParser(payerIdParser, payerWithoutIdParser);

export default payersParser