import {field, object, Parser, recursive} from "ununknown";
import urlParser from "./urlParser";

interface HeadersWithLocation {
	location: URL;
}

const headersWithLocationParser: Parser<HeadersWithLocation, unknown> = recursive(() =>
	object.of({
		location: field.required("location", urlParser)
	})
);

export default headersWithLocationParser;