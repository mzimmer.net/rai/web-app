import {compose, from, Parser, thing} from "ununknown";
import {left, right} from "fp-ts/lib/Either";

const urlParser: Parser<URL, thing.is.TypeMismatchError | string> = compose(thing.is.string, from<URL, string, string>((s: string) => {
	try {
		return right(new URL(s));
	} catch (e) {
		return left(e.message);
	}
}))


export default urlParser;