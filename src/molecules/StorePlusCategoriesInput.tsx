import FiniteSearchInput, {Displayable} from "../atoms/FiniteSearchInput";
import FiniteTagInput, {Key, Keyable} from "../atoms/FiniteTagInput";
import {List, Map} from "immutable";
import {FormField} from "grommet";
import React from "react";

const renderStoreDisplay = (result: { display: string; categories: Map<Key, Displayable>; key: Key }): string =>
	result.categories.size > 0
		? `${result.display} | ${result.categories.toIndexedSeq().map((_) => _.display).join(", ")}`
		: result.display;

export interface WithCategories {
	categories: Map<Key, Displayable>;
}

interface StorePlusCategories extends Keyable, Displayable, WithCategories {
	originalStore: Keyable & Displayable;
}

interface Props {
	store: {
		autoFocus?: boolean;
		value: Displayable | null;
		onChange: (value: Keyable & Displayable | null) => void;
		search: (searchTerm: string) => Promise<Map<Key, Displayable & WithCategories>>;
	};
	categories: {
		value: Map<Key, Displayable>;
		onAdd: (entry: Keyable & Displayable) => void;
		onAddMultiple: (entries: Map<Key, Displayable>) => void;
		onRemove: (entry: Keyable & Displayable) => void;
		search: (searchTerm: string) => Promise<Map<Key, Displayable>>;
	};
}

const StorePlusCategoriesInput: React.FC<Props> = (props) => {
	const storeOnChange = (value: StorePlusCategories | null): void => {
		props.store.onChange(value !== null ? value.originalStore : null);
		if (value !== null) {
			props.categories.onAddMultiple(value.categories);
		}
	};

	const storeSearch = (searchTerm: string): Promise<List<StorePlusCategories>> =>
		props.store.search(searchTerm).then(results => results
			.toSeq()
			.map((value, key) => ({
				key: key,
				...value
			}))
			.map(result => ({
				key: JSON.stringify({store: result.key, categories: result.categories.toList()}) as Key,
				display: renderStoreDisplay(result),
				originalStore: result,
				categories: result.categories
			}))
			.toList()
		);

	return <>
		<FormField name="store"
				   label="Store">
			<FiniteSearchInput<StorePlusCategories> value={props.store.value}
													onChange={storeOnChange}
													search={storeSearch}/>
		</FormField>
		<FormField name="categories"
				   label="Categories">
			<FiniteTagInput<object> {...props.categories}/>
		</FormField>
	</>;
};

export default StorePlusCategoriesInput;