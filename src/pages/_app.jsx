import "normalize.css/normalize.css"
import React from "react";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const RaiApp = ({Component, pageProps}) => <React.StrictMode><Component {...pageProps} /></React.StrictMode>

export default RaiApp