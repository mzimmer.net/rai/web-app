import Api, {Payers} from "../api/Api";
import {Box, Tab, Tabs, Text} from "grommet";
import {Either, isLeft} from "fp-ts/lib/Either";
import {field, object, Parser, recursive, runParser} from "ununknown";
import DefaultTemplate from "../templates/DefaultTemplate";
import ManagePayers from "../organisms/ManagePayers";
import {NextPage} from "next"
import payersParser from "../parsers/payersParser";
import payersUnparser from "../unparsers/payersUnparser";
import React from "react";

interface Props {
	preloadedPayers: Payers;
}

const propsParser: Parser<Props, unknown> = recursive(() =>
	object.of({
		preloadedPayers: field.required("preloadedPayers", payersParser)
	})
);

const propsUnparser = (props: Props): unknown => ({
	preloadedPayers: payersUnparser(props.preloadedPayers)
});

const ManageHouseHoldPage: NextPage<unknown> = (unparsedProps) => {
	const parsedProps: Either<unknown, Props> = runParser(propsParser, unparsedProps);

	if (isLeft(parsedProps)) {
		return <DefaultTemplate>
			<Box>
				<Text>An error occurred: {parsedProps.left}</Text>
			</Box>
		</DefaultTemplate>
	}

	const props = parsedProps.right;

	return <DefaultTemplate>
		<Tabs>
			<Tab title={"Manage payers"}>
				<ManagePayers preloadedPayers={props.preloadedPayers}/>
			</Tab>
		</Tabs>
	</DefaultTemplate>;
};

ManageHouseHoldPage.getInitialProps = async (): Promise<unknown> => {
	const preloadedPayers: Payers = await Api.INSTANCE.payers.index();
	const props: Props = {preloadedPayers: preloadedPayers};
	return propsUnparser(props);
}

export default ManageHouseHoldPage