import {Box, Heading} from "grommet";
import DefaultTemplate from "../templates/DefaultTemplate";
import EnterReceiptForm from "../organisms/EnterReceiptForm";
import {NextPage} from "next"
import React from "react";

const EnterReceiptPage: NextPage = () =>
	<DefaultTemplate>
		<Heading level="2">Enter receipt</Heading>
		<Box width="large">
			<EnterReceiptForm/>
		</Box>
	</DefaultTemplate>;

export default EnterReceiptPage