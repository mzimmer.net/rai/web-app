import Dashboard from "../organisms/Dashboard";
import DefaultTemplate from "../templates/DefaultTemplate";
import {NextPage} from "next"
import React from "react";

const IndexPage: NextPage = () => <DefaultTemplate><Dashboard/></DefaultTemplate>;

export default IndexPage