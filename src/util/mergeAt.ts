import {flip} from "./flip";
import {Map} from "immutable";
import {transformAt} from "./transformAt";

export const mergeAt: <K, V>(map: Map<K, V>) => (key: K, update: Partial<V>) => Map<K, V> =
	flip(transformAt)((current, update) => ({...current, ...update}));