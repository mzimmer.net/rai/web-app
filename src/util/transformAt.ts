import {Map} from "immutable"

export const transformAt =
	<K, V, P = undefined>(map: Map<K, V>) =>
		(transformer: (value: V, payload: P) => V) =>
			(key: K, payload: P): Map<K, V> =>
				map.map((v, k) => k === key ? transformer(v, payload) : v)