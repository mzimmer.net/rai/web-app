import {Box, CheckBox, Grid, Grommet, Header, Heading, Main, Text} from "grommet";
import React, {useState} from "react"
import MainMenu from "../organisms/MainMenu";
import theme from "../atoms/theme";

const DefaultTemplate: React.FC = (props) => {

	const [themeMode, setThemeMode] = useState<"light" | "dark">("light")
	const themeModeCheckBoxOnChange: React.ChangeEventHandler<HTMLInputElement> = event => setThemeMode(event.target.checked ? "dark" : "light")

	return <Grommet full theme={theme} themeMode={themeMode}>
		<Grid fill
			  rows={["auto", "flex"]}
			  columns={["auto", "flex"]}
			  areas={[
				  {name: "header", start: [0, 0], end: [1, 0]},
				  {name: "sidebar", start: [0, 1], end: [0, 1]},
				  {name: "main", start: [1, 1], end: [1, 1]}
			  ]}>
			<Header gridArea="header"
					direction="row"
					align="center"
					justify="between"
					pad={{horizontal: "medium", vertical: "small"}}
					background={{color: themeMode === 'light' ? "#c4935a" : "#603b03"}}
					elevation="medium">
				<Heading level={1} margin={{top: "0", bottom: "0"}}>Rai</Heading>
				<Text>Budgeting</Text>
				<CheckBox checked={themeMode === 'dark'} toggle={true} onChange={themeModeCheckBoxOnChange} label="Dark mode"/>
			</Header>
			<Box tag={"aside"}
				 gridArea="sidebar"
				 width="small"
				 background={{color: "brand"}}
				 elevation="medium">
				<MainMenu/>
			</Box>
			<Main gridArea="main"
				  align="center"
				  pad="small">
				{props.children}
			</Main>
		</Grid>
	</Grommet>;
}

export default DefaultTemplate