import {Box, Text, TextInput} from "grommet";
import React, {ReactElement, ReactNode, useState} from "react";
import {List} from "immutable";

export interface Displayable {
	display: string;
}

const renderLabel = (value: Displayable): ReactNode =>
	<Box pad="small"><Text>{value.display}</Text></Box>;

const renderSuggestions = <T, >(suggestions: List<T & Displayable>): Array<{ label: ReactNode; value: T & Displayable }> =>
	suggestions.map(suggestion => ({
		label: renderLabel(suggestion),
		value: suggestion
	})).toArray();

interface Props<T> {
	autoFocus?: boolean;
	value: Displayable | null;
	onChange: (value: T & Displayable | null) => void;
	search: (searchTerm: string) => Promise<List<T & Displayable>>;
}

const FiniteSearchInput = <T, >({autoFocus = false, value, onChange, search}: Props<T>): ReactElement => {
	const [input, setInput] = useState<null | string>("")
	const [suggestions, setSuggestions] = useState<List<T & Displayable>>(List());

	const _onChange: React.ChangeEventHandler<HTMLInputElement> =
		event => {
			if (input === null) {
				onChange(null);
			}
			setInput(event.target.value);
			search(event.target.value).then(response => {
				setSuggestions(response);
			});
		}

	const onSelect: (_: { suggestion: { value: T & Displayable } }) => void =
		({suggestion: {value}}) => {
			setInput(null);
			onChange(value);
		}

	const _value: string = input === null ? (value !== null ? value.display : "") : input;

	return <TextInput autoFocus={autoFocus}
					  value={_value}
					  onChange={_onChange}
					  onSelect={onSelect}
					  suggestions={renderSuggestions(suggestions)}
					  style={{border: 0}}/>;
};

export default FiniteSearchInput