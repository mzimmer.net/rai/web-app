import {Box, Button, Text} from "grommet";
import {FormClose} from "grommet-icons";
import React from "react";

interface Props {
	onRemove?: () => void;
}

const Tag: React.FC<Props> = ({onRemove, children, ...rest}) => {
	const tag = <Box
		direction="row"
		align="center"
		background="brand"
		pad={{horizontal: "xsmall", vertical: "xxsmall"}}
		margin={{horizontal: "xxsmall"}}
		round="medium"
		{...rest}>
		<Text size="xsmall" margin={{right: "xxsmall"}}>
			{children}
		</Text>
		{onRemove !== undefined && <FormClose size="small" color="text"/>}
	</Box>;
	return onRemove !== undefined ? <Button onClick={onRemove}>{tag}</Button> : tag;
}

export default Tag