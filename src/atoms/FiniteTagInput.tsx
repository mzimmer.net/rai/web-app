import FiniteSearchInput, {Displayable} from "./FiniteSearchInput";
import {List, Map} from "immutable";
import React, {ReactElement} from "react";
import {Box} from "grommet";
import Tag from "./Tag";

export type Key = string & { readonly __tag: unique symbol };

export interface Keyable {
	key: Key;
}

interface Props<T> {
	value: Map<Key, T & Displayable>;
	onAdd: (entry: T & Keyable & Displayable) => void;
	onRemove: (entry: T & Keyable & Displayable) => void;
	search: (searchTerm: string) => Promise<Map<Key, T & Displayable>>;
}

const FiniteTagInput = <T, >({value, onAdd, onRemove, search}: Props<T>): ReactElement => {
	const onChange = (entry: T & Keyable & Displayable | null): void => {
		if (entry !== null) {
			if (!value.has(entry.key)) {
				onAdd(entry);
			}
		}
	}

	const _search = (searchTerm: string): Promise<List<T & Keyable & Displayable>> =>
		search(searchTerm).then(results =>
			results.toKeyedSeq()
				.filter((_, key) => !value.has(key))
				.map((value, key) => ({key, ...value}))
				.toList()
		);

	return <>
		<Box direction="row-responsive">
			{value.map((value, key) =>
				<Tag key={key}
					 onRemove={(): void => onRemove({key, ...value})}>
					{value.display}
				</Tag>).toList()}
		</Box>
		<FiniteSearchInput<T & Keyable & Displayable> value={null}
													  onChange={onChange}
													  search={_search}/>
	</>;
}

export default FiniteTagInput