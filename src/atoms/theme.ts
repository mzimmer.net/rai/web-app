const theme: object = {
	global: {
		colors: {
			brand: "#91652f",
			background: {
				dark: "#111111",
				light: "#ffffff"
			},
			"background-back": {
				dark: "#111111",
				light: "#eeeeee"
			},
			"background-front": {
				dark: "#222222",
				light: "#ffffff"
			},
			"background-contrast": {
				dark: "#ffffff11",
				light: "#11111111"
			},
			text: {
				dark: "#eeeeee",
				light: "#333333"
			},
			"text-strong": {
				dark: "#ffffff",
				light: "#000000"
			},
			"text-weak": {
				dark: "#cccccc",
				light: "#444444"
			},
			"text-xweak": {
				dark: "#999999",
				light: "#666666"
			},
			border: {
				dark: "#444444",
				light: "#cccccc"
			},
			control: "#39423c",
			"active-background": "background-contrast",
			"active-text": "text-strong",
			"selected-background": "brand",
			"selected-text": "text-strong",
			focus: {
				dark: "#44444488",
				light: "#cccccc88"
			},
			"status-critical": "#ff4040",
			"status-warning": "#ffaa15",
			"status-ok": "#00c781",
			"status-unknown": "#cccccc",
			"status-disabled": "#cccccc"
		},
		font: {
			family: "Arial"
		}
	},
	radioButton: {
		check: {
			color: {
				light: "control",
				dark: "brand"
			}
		}
	}
}

export default theme;