import {Box, FormField, Text, TextInput} from "grommet";
import React, {KeyboardEventHandler, useState} from "react";

const moneyAsString = (amount: bigint | null, fixed: number, symbol: string): string => `${((amount !== null ? Number(amount) : 0) / Math.pow(10, fixed)).toFixed(fixed)} ${symbol}`;

const parseInput = (value: string): bigint | null => {
	try {
		return value.length === 0 ? null : BigInt(value);
	} catch (e) {
		return null;
	}
}

interface MoneyInputProps {
	name?: string;
	label?: string | React.ReactNode;
	allowNegative?: boolean;
	value: bigint;
	onChange: (value: bigint) => void;
	onKeyDown?: KeyboardEventHandler<HTMLInputElement>;
}

const MoneyInput: React.FC<MoneyInputProps> = (
	{
		name,
		label,
		allowNegative = true,
		value,
		onChange,
		onKeyDown
	}) => {
	const [input, setInput] = useState<string | null>("");

	const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> =
		event => {
			const parsed = parseInput(event.target.value);
			setInput(parsed !== null ? null : event.target.value);
			onChange(parsed !== null ? parsed : BigInt(0));
		}

	const actualValue = input === null && value !== undefined ? value : BigInt(0)

	const displayValue = moneyAsString(actualValue, 2, '€');

	const allowNegativeProp = allowNegative ? {} : {min: 0};

	return <Box>
		<FormField name={name} label={label}>
			<TextInput type="number"
					   style={{textAlign: "end"}}
					   value={input === null ? Number(value) : input}
					   onChange={onChangeHandler}
					   onKeyDown={onKeyDown}
					   {...allowNegativeProp}/>
		</FormField>
		<Text alignSelf="end">{displayValue}</Text>
	</Box>;
}
export default MoneyInput